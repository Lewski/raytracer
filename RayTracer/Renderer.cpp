#include "Renderer.h"

Renderer::Renderer(Scene* scene) {
	renderMethod = RenderMethods::VULKAN;
	currentScene = scene;

	openGlRenderer = OpenGLRenderer();
	vulkanRenderer = VulkanRenderer(scene);
}

Renderer::~Renderer() {

}

void Renderer::init() {
	initWindow();
	initRenderer();
}

void Renderer::initWindow() {

	glfwInit();

	window.reset(glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", nullptr, nullptr));

	glfwSetKeyCallback(window.get(), Input::keyCallback);
	glfwSetCursorPosCallback(window.get(), Input::cursorPosCallback);
	glfwSetInputMode(window.get(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	if (renderMethod == RenderMethods::VULKAN) {
		vulkanRenderer.initWindow(window.get());
	}

}
void Renderer::initRenderer() {
	if (renderMethod == RenderMethods::OPEN_GL) {
		openGlRenderer.width = WIDTH;
		openGlRenderer.height = HEIGHT;
		openGlRenderer.run();

	}
	else if(renderMethod == RenderMethods::VULKAN) {
		vulkanRenderer.width = WIDTH;
		vulkanRenderer.height = HEIGHT;
		vulkanRenderer.initVulkan();
	}
}
void Renderer::mainLoop() {
	while (!glfwWindowShouldClose(window.get())) {
		auto startTime = std::chrono::high_resolution_clock::now();
		glfwPollEvents();

		Input::update();
		currentScene->update(frameTime);

		if (renderMethod == RenderMethods::VULKAN) {
			vulkanRenderer.update(frameTime);
		}

		auto currentTime = std::chrono::high_resolution_clock::now();
		frameTime = std::chrono::duration<double, std::chrono::nanoseconds::period>(currentTime - startTime).count() / 1000000.0;
	}

	if (renderMethod == RenderMethods::VULKAN) {
		vulkanRenderer.end();
	}


}
void Renderer::cleanup() {

}