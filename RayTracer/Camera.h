#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include "InputManager.h"

class Camera
{

public:

	void update(double dt);

	glm::vec3 pos = glm::vec3(0.0f, 0.0f, 2.0f);
	glm::vec3 lookDir = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

};

