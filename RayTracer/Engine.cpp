#include <stdio.h>
#include <stdlib.h>

#include "Engine.h"

void Engine::run() {
	currentScene = new Scene();
	renderer = Renderer(currentScene);
	renderer.init();
}

void Engine::init() {
	
}

void Engine::mainLoop() {

}

void Engine::cleanup() {

}
