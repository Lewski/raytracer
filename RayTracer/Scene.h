#pragma once

#include "Camera.h"
#include "Mesh.h"

class Scene
{

public:

	Scene() { testMesh = new Mesh(); }

	void update(double dt);

	Mesh* testMesh;
	Camera* mainCamera;

};

