#pragma once

#define GLEW_STATIC
#include<GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <vector>


class OpenGLRenderer
{

public:

	void run();

	uint32_t width, height;

private:

	//private methods

	bool initWindow();
	bool initOpenGL();
	void mainLoop();
	void cleanup();
	GLuint loadShaders(const char* vertexFilePath, const char* fragmentFilePath);

	GLFWwindow* window;
	GLuint VertexArrayID;
	GLuint vertexBuffer;
	GLuint shaderProgramID;

};

