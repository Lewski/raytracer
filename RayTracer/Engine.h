#pragma once

#include "Renderer.h"
#include "Scene.h"

class Engine
{

public:

	Engine() {}

	void run();

private:

	void init();
	void mainLoop();
	void cleanup();

	Renderer renderer;
	Scene* currentScene;
};

