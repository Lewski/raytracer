#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include <algorithm>
#include <cstdint>
#include <iostream>

#include "OpenGLRenderer.h"
#include "VulkanRenderer.h"

const uint32_t WIDTH = 1600;
const float ASPECT = 16.0f / 9.0f;
const uint32_t HEIGHT = WIDTH / ASPECT;

class Renderer
{

public:

	Renderer() {}
	Renderer(Scene* scene);
	~Renderer();

	enum RenderMethods {
		VULKAN,
		OPEN_GL,
		RAY_TRACE
	} renderMethod;

	void init();

	double frameTime;

private:

	//private methods

	void initWindow();
	void initRenderer();
	void mainLoop();
	void cleanup();

	std::shared_ptr<GLFWwindow> window;
	Scene* currentScene;
	OpenGLRenderer openGlRenderer;
	VulkanRenderer vulkanRenderer;

};

