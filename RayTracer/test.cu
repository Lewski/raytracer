#include "test.cuh"

#include <iostream>

__global__ void testKernel(void) {
	printf("Hello world from thread [%d,%d] \ From device\n", threadIdx.x, blockIdx.x);
	
}

namespace Cuda {

	void testHost(void) {
		int a = 0;
		a += 2;
	}

	void cudaTest(void) {
		printf("Hello world from host!\n");
		bool useCuda = true;
		int numberOps = 2;
		if (useCuda) {
			testKernel << <3, 3 >> > ();
			cudaDeviceSynchronize();
			std::cout << "kernel done" << std::endl;
		}
		else {
			for (int i = 0; i < numberOps; i++) {
				testHost();
			}
			std::cout << "host done" << std::endl;
		}
		

		
	}
}
 